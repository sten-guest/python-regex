Source: python-regex
Section: python
Priority: optional
Maintainer: Sandro Tosi <morph@debian.org>
Uploaders: Debian Python Modules Team <python-modules-team@lists.alioth.debian.org>
Build-Depends: debhelper (>= 10), python-all (>= 2.6.6-3~), python-all-dev, python-all-dbg, python3-all-dev, python3-all-dbg, python-docutils, dh-python, python-pygments
Standards-Version: 4.1.2
X-Python-Version: all
X-Python3-Version: >= 3.2
Homepage: https://bitbucket.org/mrabarnett/mrab-regex
Vcs-Git: https://anonscm.debian.org/git/python-modules/packages/python-regex.git
Vcs-Browser: https://anonscm.debian.org/cgit/python-modules/packages/python-regex.git

Package: python-regex
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, ${python:Depends}
Description: alternative regular expression module
 This new regex implementation is intended eventually to replace Python's
 current re module implementation.
 .
 For testing and comparison with the current 're' module the new implementation
 is in the form of a module called 'regex'.

Package: python-regex-dbg
Section: debug
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}, python-all-dbg, python-regex (= ${binary:Version})
Description: alternative regular expression module (debug extension)
 This new regex implementation is intended eventually to replace Python's
 current re module implementation.
 .
 For testing and comparison with the current 're' module the new implementation
 is in the form of a module called 'regex'.
 .
 This package contains the debug extension for python-regex.

Package: python3-regex
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}, ${python3:Depends}
Description: alternative regular expression module (Python 3)
 This new regex implementation is intended eventually to replace Python's
 current re module implementation.
 .
 For testing and comparison with the current 're' module the new implementation
 is in the form of a module called 'regex'.
 .
 This is the Python 3 version of the package.

Package: python3-regex-dbg
Section: debug
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}, python3-all-dbg, python3-regex (= ${binary:Version})
Description: alternative regular expression module (Python 3 debug extension)
 This new regex implementation is intended eventually to replace Python's
 current re module implementation.
 .
 For testing and comparison with the current 're' module the new implementation
 is in the form of a module called 'regex'.
 .
 This package contains the debug extension for python3-regex.
